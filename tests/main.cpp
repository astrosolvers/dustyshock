#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "common_use.hpp"
#include "cells_shock.hpp"
#include "monaghan_shock.hpp"

int main()
{
    // create directory to output data
    sprintf(DATA_DIR, ".%sparticles", DIR_SEPARATOR);

    struct stat st;
    if (stat(DATA_DIR, &st) == -1) {
        create_directory(DATA_DIR);
    }

    ProblemParams problemParams;
    problemParams.T = 0.2;
    problemParams.h = 0.01;
    problemParams.tau = 0.001;
    //problemParams.t_stop = 0.002;
    problemParams.K = 500;
    problemParams.d2g = 1;

    problemParams.gamma = 1.4;
    problemParams.membrane = 0;
    problemParams.left = -0.5;
    problemParams.right = 0.5;
    problemParams.haveViscosity = true;
    problemParams.alfa = 1;
    problemParams.beta = 2;
    problemParams.nu_coef = 0.1;

    ParticleParams gasParams;
    gasParams.particles_amount = 990;  // should be devidable by (L2R + 1)
    gasParams.im_particles_amount = 450;  // should be devidable by (L2R + 1)
    gasParams.isGas = true;

    gasParams.rho_left = 100;
    gasParams.press_left = 100;
    gasParams.vel_left = 0;
    gasParams.energy_left = 2.5;

    gasParams.rho_right = 12.5;
    gasParams.press_right = 10;
    gasParams.vel_right = 0;
    gasParams.energy_right = 2;

    ParticleParams dustParams;
    dustParams.particles_amount = 990;
    dustParams.im_particles_amount = 450;
    dustParams.isGas = false;

    dustParams.rho_left = 100 * problemParams.d2g;
    dustParams.vel_left = 0;
    dustParams.rho_right = 12.5 * problemParams.d2g;
    dustParams.vel_right = 0;

    double cells_length = 0.005;

    // check correctness of params
    double l2r = gasParams.rho_left / gasParams.rho_right;
    assert(is_close_to_int(fmod(gasParams.particles_amount, l2r + 1), __DOUBLE_ROUNDING_EPS));
    assert(is_close_to_int(fmod(gasParams.im_particles_amount, l2r + 1), __DOUBLE_ROUNDING_EPS));

    clock_t startTime = clock();

    cells_shock(cells_length, gasParams, dustParams, problemParams);
    monaghan_shock(gasParams,dustParams, problemParams);

    clock_t finishTime = clock();

    double executionTime = (double)(finishTime - startTime) / CLOCKS_PER_SEC;
    printf("Finished in %lf seconds.\n", executionTime);

    return 0;
}