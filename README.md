1. Run one of the scripts:

(Cmake is required)

  OS Windows:
      run_cmake_win.cmd
    It produces a solution for Visual Studio into ./build directory. 
    Open the solution and build test app. 

  OS Linux:
      run_cmake_linux.sh
    It produces a makefile in ./build directory and runs 'make'

The resulting executable will be placed into ./bin directory.

2. Parameters could be set directly in 'main.cpp' file as hard-coded values
(see initialization of structures: problemParams, gasParams, dustParams)

3. Run executable

4. The output will be written to the ./bin/particles directory.

